<?php

require_once 'vendor/autoload.php';

use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;

function getReader(String $ext)
{
    return ReaderFactory::create($ext);
}


function getWriter(String $ext)
{
    return WriterFactory::create($ext);
}

function getExtension(String $fileName)
{
    $extArr     = explode('.', $fileName);
    $ext        = strtolower(end($extArr));

    if (!in_array($ext, ['csv', 'xlsx', 'ods'])) {
        throw new Exception("File Not Support");
    }

    return $ext;
}

function getFileName(String $file)
{
    $arr = array_filter(explode('/', $file));

    return end($arr);
}


$arguments      = getopt('f:c:d:',['file:','column:','destination:']);
$file           = $arguments['f'] ?? $arguments['file'];
$columns        = $arguments['c'] ?? $arguments['column'];
$destination    = $arguments['d'] ?? $arguments['destination'] ?? "output/";

if (!file_exists($file)) {
    throw new Exception("File doesn't exists", 500);
}

$columns        = array_filter(explode(',', strtolower($columns)));
$fileName       = getFileName($file);
$ext            = getExtension($file);
$reader         = getReader($ext);
$writer         = getWriter($ext);
$destination    = array_filter(explode('/', $destination));

if (!file_exists($dir = implode('/', $destination))) {
    if (!@mkdir($dir, 0777)) {
        throw new Exception("Cant Create destination folder", 403);
    }
}

$destination[]  = $fileName;
$destination    = implode('/', $destination);

$reader->open($file);
$writer->openToFile($destination);

foreach ($reader->getSheetIterator() as $sheet) {
    $rowIterator    = $sheet->getRowIterator();
    $rowIterator->next();

    $headers        = $rowIterator->current();
    $newHeader      = array_filter($headers, function($item) use ($columns) {
                        return in_array(strtolower($item), $columns);
                    });

    foreach ($sheet->getRowIterator() as $row) {
        $newRow = array_intersect_key($row, $newHeader);
        $writer->addRow($newRow);
    }
}

$reader->close();
$writer->close();
