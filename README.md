# Filter-Columns-Tools

### Getting Start:
Clone the project repository

```sh
$ git clone https://bitbucket.org/muhzal/filter-columns-tools
```
Install dependency
```sh
$ cd filter-columns-tools
$ composer install
```

### How to use:
```sh
$ php GetSomeColumns.php -f files/example.csv -c column1,column2,colum3 -d output/
```